/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
dojo.registerModulePath("segnalazioni", "../../../Javascript/Segnalazione");

dojo.provide("segnalazioni.TblControl");

dojo.declare("segnalazioni.TblControl", null, {
    table: null,
    valueMatrix: null, //Creo Matrice valori da passare a funzione di Filippo che disegna tabella
    idsMatrix: null, //Creo Matrice degli id da passare a funzione di Filippo (non sarebbe indispensabile ma se non do id poi divento matta a cercare le celle)
    lblModRicezione: null,
    lblProtocollo: null,
    ddlModRicezione: null,
    tbProtocollo_N:null,
    wdtProtocollo_Del: null,
    divProtocollo: null,
    lblSoggetto: null,
    tblSoggetto: null,
    lblNome: null,
    lblCognome: null,
    tbNome: null,
    tbCognome: null,
    lblQualita: null,
    tbQualita: null,
    lblTelefono: null,
    lblFax: null,
    tbTelefono: null,
    tbFax: null,
    lblMail: null,
    tbMail: null,
    lblSegnalazione: null,
    lblTestoSegnalazione: null,
    tbTestoSegnalazione: null,
    lblPriorita: null,
    ddlPriorita: null,
    lblSolleciti: null,
    arrObjSolleciti: null,
    lblAllegati: null,
    lblAggiungiAllegati: null,
    cntlAllegati: null,
    lblValidazione: null,
    lblStatoValidazione: null,
    ddlStatoValidazione: null,    
    divInizioPrevisto: null,
    divFinePrevista: null,
    divInizioEffettivo: null,
    divFineEffettiva: null,
    lblNoteValidazione: null,
    tbNoteValidazione: null,
    lblSoluzione: null,
    lblTestoSoluzione: null,
    tbTestoSoluzione: null,
    lblAssegnaA: null,
    ddlAssegnaA: null,
    lblStatoSoluzione: null,
    ddlStatoSoluzione: null,
    lblAssegnazione: null,
    lblIndirizzo: null, 
    tbIndirizzo:null, 
    divCivico:null, 
    tbCivico: null, 
    lblEsponente: null, 
    tbEsponente: null,
    lblIndirizzoDescrizione: null, 
    tbIndirizzoDescrizione: null,
    lblIndirizzoCategoria:null, 
    tbIndirizzoCategoria:null, 
    lblIndirizzoSottoCategoria:null, 
    tbIndirizzoSottoCategoria:null,
    lblLocalizzazioneInt: null,
    tbIndirizzoSottoCategoria_Strada:null,
    tbIndirizzoSottoCategoria_Verde:null,
    tbIndirizzoSottoCategoria_Sicurezza:null,
    tbIndirizzoSottoCategoria_Sindaco:null,
    constructor:function(){
        this.valueMatrix =new Array();
        this.idsMatrix = new Array();
        this.idsMatrix.push("tblControlSegnalazione") // il 1° elemento della matrice degli Id è Id tabella
         
        //PROTOCOLLO
        this.lblModRicezione = dojo.create("span", {
            "innerHTML":"Mod. Ricezione"
        })
        this.lblProtocollo= dojo.create("span", {
            "innerHTML":"N° / Prot. del"
        })
        this.divProtocollo = dojo.create("div", {
            "id":"divProtocollo"
        })
        this.wdtProtocollo_Del = igedit_getById(JS_UDT_RICEZIONE[0][2].id).Element;
        this.tbProtocollo_N = JS_UDT_RICEZIONE[0][1]
        this.ddlModRicezione = JS_UDT_RICEZIONE[0][0]
        dojo.place(this.tbProtocollo_N, this.divProtocollo, "first")
        dojo.place(this.wdtProtocollo_Del, this.divProtocollo, "last")
         
        this.idsMatrix.push(new Array("trRicezione","lblModRicezione", "ddlModRicezione", "lblProtocollo", "tbProtocollo"))
        this.valueMatrix.push(new Array(this.lblModRicezione, this.ddlModRicezione, this.lblProtocollo, this.divProtocollo))
         
        //////////////////////////////////////////////////////////////////////////
        //SOGGETTO INTESTAZIONE
         
        this.idsMatrix.push(new Array("trSoggetto","lblSoggetto"))
         
        this.lblSoggetto = dojo.create("span", {
            "innerHTML":"<hr/><br/>Segnalatore", 
            "class":"lblTitolo"
        })
        this.valueMatrix.push(new Array(this.lblSoggetto))
         
        ////////////////////////////////////////////////////////////////////////////
        //SOGGETTO DATI ANAGRAFICI
        this.idsMatrix.push(new Array("trSoggettoAnagrafica", "lblNome", "tbNome", "lblCognome", "tbCognome"))
        this.lblNome = dojo.create("span", {
            "innerHTML":"Nome"
        })
        this.lblCognome = dojo.create("span", {
            "innerHTML": "Cognome"
        })
        this.tbNome = JS_UDT_ANAGRAFICASEGNALATORE[0][0]
        this.tbCognome = JS_UDT_ANAGRAFICASEGNALATORE[0][1]
        this.valueMatrix.push(new Array(this.lblNome, this.tbNome, this.lblCognome, this.tbCognome))
         
        /////////////////////////////////////////////////////////////////////////
        //SOGGETTO TIPO SOGGETTO
        this.idsMatrix.push(new Array("trSoggettoQualita", "lblQualita", "tbQualita"))
        this.lblQualita = dojo.create("span", {
            "innerHTML":"Tipo soggetto"
        })
        this.tbQualita = JS_UDT_ANAGRAFICASEGNALATORE[0][2]
         
        this.valueMatrix.push(new Array(this.lblQualita, this.tbQualita))
        //////////////////////////////////////////////////////////////////////////
        //SOGGETTO RIFERIMENTI TELEFONICI
         
        this.idsMatrix.push(new Array("trRifTelefonici","lblTelefono", "tbTelefono", "lblFax", "tbFax"))
        this.lblTelefono = dojo.create("span", {
            "innerHTML":"Telefono"
        })
        this.tbTelefono = JS_UDT_ANAGRAFICASEGNALATORE[0][3]
        this.lblFax = dojo.create("span", {
            "innerHTML":"Fax"
        })
        this.tbFax = JS_UDT_ANAGRAFICASEGNALATORE[0][4]
         
        this.valueMatrix.push(new Array(this.lblTelefono, this.tbTelefono, this.lblFax, this.tbFax))
         
        ////////////////////////////////////////////////////////////////////////////
        //SOGGETTO EMAIL
        this.idsMatrix.push(new Array("trMail", "lblMail", "tbMail"))
        this.lblMail = dojo.create("span", {
            "innerHTML":"Email"
        })
        this.tbMail = JS_UDT_ANAGRAFICASEGNALATORE[0][5]
        this.valueMatrix.push(new Array(this.lblMail, this.tbMail))
        
        //////////////////////////////////////////////////////////////////////////
        //LOCALIZZAZIONE INTESTAZIONE
         
        this.idsMatrix.push(new Array("trLocalizzazione","lblLocalizzazione"))
         
        this.lblLocalizzazioneInt = dojo.create("span", {
            "innerHTML":"<hr/><br/>Localizzazione", 
            "class":"lblTitolo"
        })
        this.valueMatrix.push(new Array(this.lblLocalizzazioneInt))
        
        ////////////////////////////////////////////////////////////////////////////
        //LOCALIZZAZIONE
        this.tbIndirizzo = JS_UDT_LOCALIZZAZIONE[0][0]
        this.tbCivico = JS_UDT_LOCALIZZAZIONE[0][1]
        this.tbEsponente = JS_UDT_LOCALIZZAZIONE[0][2]
        this.idsMatrix.push(new Array("trIndirizzo", "lblIndirizzo", "tbIndirizzo", "divCivico"))
        this.lblIndirizzo = dojo.create("span", {
            "innerHTML":"Indirizzo"
        })
        this.divCivico = dojo.create("div", {
            "innerHTML": "Civico"
        })
        this.lblEsponente = dojo.create("span", {
            "innerHTML": "/"
        })
        dojo.place(this.tbCivico, this.divCivico, "last")
        
        dojo.place(this.lblEsponente, this.divCivico, "last")
        
        dojo.place(this.tbEsponente, this.divCivico, "last")
        this.valueMatrix.push(new Array(this.lblIndirizzo, this.tbIndirizzo, this.divCivico))
         
        
        ////////////////////////////////////////////////////////////////////////////
        //LOCALIZZAZIONE DESCRIZIONE
        this.idsMatrix.push(new Array("trIndirizzoDescrizione", "lblIndirizzoDescrizione", "tbIndirizzoDescrizione"))
        this.lblIndirizzoDescrizione = dojo.create("span", {
            "innerHTML":"Descrizione localizzazione"
        })
      
        this.tbIndirizzoDescrizione = JS_UDT_LOCALIZZAZIONE[0][3]
       
        this.valueMatrix.push(new Array(this.lblIndirizzoDescrizione, this.tbIndirizzoDescrizione))
        
        ////////////////////////////////////////////////////////////////////////////
        //LOCALIZZAZIONE CATEGORIA
        this.idsMatrix.push(new Array("trIndirizzoCategoria", "lblIndirizzoCategoria", "tbIndirizzoCategoria", "lblIndirizzoSottoCategoria", "divIndirizzoSottoCategoria"))
        this.lblIndirizzoCategoria = dojo.create("span", {
            "innerHTML":"Categoria"
        })
        
        this.lblIndirizzoSottoCategoria = dojo.create("span", {
            "innerHTML":"Sottocategoria"
        })
        this.divIndirizzoSottoCategoria = dojo.create("div")
        
        this.tbIndirizzoCategoria = JS_UDT_LOCALIZZAZIONE[0][4]
               
        this.tbIndirizzoSottoCategoria_Strada = JS_UDT_LOCALIZZAZIONE[0][5]
        this.tbIndirizzoSottoCategoria_Verde = JS_UDT_LOCALIZZAZIONE[0][6]
        this.tbIndirizzoSottoCategoria_Sicurezza = JS_UDT_LOCALIZZAZIONE[0][7]
        this.tbIndirizzoSottoCategoria_Sindaco = JS_UDT_LOCALIZZAZIONE[0][8]
        
        dojo.place(this.tbIndirizzoSottoCategoria_Strada, this.divIndirizzoSottoCategoria, "first")
        dojo.place(this.tbIndirizzoSottoCategoria_Verde, this.divIndirizzoSottoCategoria, "first")
        dojo.place(this.tbIndirizzoSottoCategoria_Sicurezza, this.divIndirizzoSottoCategoria, "first")
        dojo.place(this.tbIndirizzoSottoCategoria_Sindaco, this.divIndirizzoSottoCategoria, "first")
        this.valueMatrix.push(new Array(this.lblIndirizzoCategoria, this.tbIndirizzoCategoria, this.lblIndirizzoSottoCategoria, this.divIndirizzoSottoCategoria))
         
        
      
        
        dojo.connect(this.tbIndirizzoCategoria,"onchange", this.ShowHideSottocategoria);
        //////////////////////////////////////////////////////////////////////////
        //SEGNALAZIONE INTESTAZIONE
         
        this.idsMatrix.push(new Array("trSoggetto","lblSegnalazione"))
         
        this.lblSegnalazione = dojo.create("span", {
            "innerHTML":"<hr/><br/>Segnalazione", 
            "class":"lblTitolo"
        })
        this.valueMatrix.push(new Array(this.lblSegnalazione))
         
        /////////////////////////////////////////////////////////////////////////
        //SEGNALAZIONE TESTO
        this.idsMatrix.push(new Array("trTestoSegnalazione", "lblTestoSegnalazione", "tbTestoSegnalazione"))
        this.lblTestoSegnalazione = dojo.create("span", {
            "innerHTML":"Testo"
        })
        this.tbTestoSegnalazione = JS_UDT_SEGNALAZIONE[0][0]
         
        this.valueMatrix.push(new Array(this.lblTestoSegnalazione, this.tbTestoSegnalazione))
         
        /////////////////////////////////////////////////////////////////////////
        //SEGNALAZIONE PRIORITA
        this.idsMatrix.push(new Array("trPrioritaSegnalazione", "lblPriorita", "ddlPriorita"))
        this.lblPriorita = dojo.create("span", {
            "innerHTML":"Priorità"
        })
        this.ddlPriorita = JS_UDT_SEGNALAZIONE[0][1]
        this.valueMatrix.push(new Array(this.lblPriorita, this.ddlPriorita))
         
        //////////////////////////////////////////////////////////////////////////
        //SOLLECITI
         
        this.idsMatrix.push(new Array("trSolleciti","lblSolleciti"))
         
        this.lblSolleciti = dojo.create("span", {
            "innerHTML":"<hr/><br/>Solleciti", 
            "class":"lblTitolo"
        })
        this.valueMatrix.push(new Array(this.lblSolleciti))
        this.arrObjSolleciti = new Array() 
        for (var i=0; i<JS_UDT_SOLLECITI.length; i++)
        {
            this.idsMatrix.push(new Array("trSollecito_"+i, "lblDataSollecito:"+i, "wdtDataSollecito_"+i))  
            var lblData = dojo.create("span", {
                "innerHTML":"Data"
            })
            this.valueMatrix.push(new Array(lblData, igedit_getById(JS_UDT_SOLLECITI[i][1].id).Element))
             
            this.idsMatrix.push(new Array("trSollecito_b"+i,"lblTestoSollecito_"+i, "tbTestoSollecito_"+i))  
            var lblTesto = dojo.create("span", {
                "innerHTML":"Testo"
            })
            this.valueMatrix.push(new Array( lblTesto, JS_UDT_SOLLECITI[i][0]))
           
            this.arrObjSolleciti.push({
                "data":igedit_getById(JS_UDT_SOLLECITI[i][1].id).Element,
                "testo":JS_UDT_SOLLECITI[i][0]
            })
   
        }
        this.idsMatrix.push(new Array("trBtnAdd", "btnAdd"))
        this.valueMatrix.push(new Array(dojo.query('input[id*=aggiungi]')[0]))
         
        //////////////////////////////////////////////////////////////////////////
        //ALLEGATI INTESTAZIONE
         
        this.idsMatrix.push(new Array("trIntestazioneAllegati","lblAllegatii"))
         
        this.lblAllegati = dojo.create("span", {
            "innerHTML":"<hr/><br/>Allegati", 
            "class":"lblTitolo"
        })
        this.valueMatrix.push(new Array(this.lblAllegati))
        
        this.idsMatrix.push(new Array("trAllegati", "lblAggiungiAllegati", 'cntlAllegati'))
        this.lblAggiungiAllegati = dojo.create("span", {
            "innerHTML": "Fare click sulla freccetta per aggiungere un file"
        })
        this.cntlAllegati = dojo.query('div[id*=pnlContainer]')[0]
        
        this.valueMatrix.push(new Array(this.lblAggiungiAllegati, this.cntlAllegati))
        
        //////////////////////////////////////////////////////////////////////////
        //VALIDAZIONE INTESTAZIONE
         
        this.idsMatrix.push(new Array("trIntestazioneValidazione","lblValidazione"))
         
        this.lblValidazione = dojo.create("span", {
            "innerHTML":"<hr/><br/>Validazione", 
            "class":"lblTitolo"
        })
        this.valueMatrix.push(new Array(this.lblValidazione))
        
        //////////////////////////////////////////////////////////////////////////
        //VALIDAZIONE STATO
        this.idsMatrix.push(new Array("trStatoValidazione", "lblStatoValidazione", "ddlStatoValidazione"))
        this.lblStatoValidazione = dojo.create("span", {
            "innerHTML": "Stato"
        })
        this.ddlStatoValidazione = JS_UDT_VALIDAZIONE[0][0]
        this.valueMatrix.push(new Array(this.lblStatoValidazione, this.ddlStatoValidazione))
        
        //////////////////////////////////////////////////////////////////////////
        //VALIDAZIONE DATE
        this.idsMatrix.push(new Array("trDateLavori", "divInizioPrevisto", "divFinePrevista", "divInizioEffettivo", "divFineEffettiva"))
        this.divInizioPrevisto = dojo.create("div", {
            "innerHTML":"Inizio previsto<br/>"
        })
        dojo.place(igedit_getById(JS_UDT_VALIDAZIONE[0][1].id).Element, this.divInizioPrevisto, "last")
         
        this.divFinePrevista = dojo.create("div", {
            "innerHTML":"Fine prevista<br/>"
        })
        dojo.place(igedit_getById(JS_UDT_VALIDAZIONE[0][2].id).Element, this.divFinePrevista, "last")
        
        this.divInizioEffettivo = dojo.create("div", {
            "innerHTML":"Inizio effettivo<br/>"
        })
        dojo.place(igedit_getById(JS_UDT_VALIDAZIONE[0][3].id).Element, this.divInizioEffettivo, "last")
         
        this.divFineEffettiva = dojo.create("div", {
            "innerHTML":"Fine effettiva<br/>"
        })
        dojo.place(igedit_getById(JS_UDT_VALIDAZIONE[0][4].id).Element, this.divFineEffettiva, "last")
        this.valueMatrix.push(new Array(this.divInizioPrevisto, this.divFinePrevista, this.divInizioEffettivo, this.divFineEffettiva))
        
        //////////////////////////////////////////////////////////////////////////
        //VALIDAZIONE DATE
        this.idsMatrix.push(new Array("trNoteValidazione", "lblNoteValidazione", "tbNoteValidazione"))
        this.lblNoteValidazione = dojo.create("span", {
            "innerHTML":"Note"
        })
        this.tbNoteValidazione = JS_UDT_VALIDAZIONE[0][5]
        this.valueMatrix.push(new Array(this.lblNoteValidazione, this.tbNoteValidazione))
        
        
        //////////////////////////////////////////////////////////////////////////
        //SOLUZIONE INTESTAZIONE
         
        this.idsMatrix.push(new Array("trIntestazioneSoluzione","lblSoluzione"))
         
        this.lblSoluzione = dojo.create("span", {
            "innerHTML":"<hr/><br/>Soluzione", 
            "class":"lblTitolo"
        })
        this.valueMatrix.push(new Array(this.lblSoluzione))
        
        //////////////////////////////////////////////////////////////////////////
        //SOLUZIONE TESTO
        
        this.idsMatrix.push(new Array("trSoluzione", "lblTestoSoluzione", "tbTestoSoluzione"))
        this.lblTestoSoluzione = dojo.create("span", {
            "innerHTML":"Testo"
        })
        this.tbTestoSoluzione = JS_UDT_SOLUZIONE[0][0]
        
        this.valueMatrix.push(new Array(this.lblTestoSoluzione, this.tbTestoSoluzione))
        
        //////////////////////////////////////////////////////////////////////////
        //SOLUZIONE INTESTAZIONE
         
        this.idsMatrix.push(new Array("trIntestazioneAssegnazione","lblAssegnazione"))
         
        this.lblAssegnazione = dojo.create("span", {
            "innerHTML":"<hr/><br/>Assegnazione", 
            "class":"lblTitolo"
        })
        this.valueMatrix.push(new Array(this.lblAssegnazione))
        
        //////////////////////////////////////////////////////////////////////////
        //SOLUZIONE ASSEGNA
        this.idsMatrix.push(new Array("trSoluzioneAssegnazione", "lblAssegnaA", "ddlAssegnaA", "lblStatoSoluzione", "ddlStatoSoluzione"))
        
        this.lblAssegnaA= dojo.create("span", {
            "innerHTML": "Assegna a"
        })
        this.ddlAssegnaA = JS_UDT_SOLUZIONE[0][1]
        
        this.lblStatoSoluzione= dojo.create("span", {
            "innerHTML": "Stato"
        })
        this.ddlStatoSoluzione = JS_UDT_SOLUZIONE[0][2]
        
        this.valueMatrix.push(new Array(this.lblAssegnaA, this.ddlAssegnaA,this.lblStatoSoluzione, this.ddlStatoSoluzione))
         
        this.table = createTable(this.valueMatrix, this.idsMatrix) 
        
    },
    
    HideCityFromsControl: function(){
        JS_ATTACHMENT_ALLEGATI_ROW.style.display = "none";
        JS_UDT_ANAGRAFICASEGNALATORE_ROW.style.display = "none";
        JS_UDT_RICEZIONE_ROW.style.display = "none";
        JS_UDT_SEGNALAZIONE_ROW.style.display = "none";
        JS_UDT_SOLLECITI_ROW.style.display = "none";
        JS_UDT_SOLUZIONE_ROW.style.display = "none";
        JS_UDT_VALIDAZIONE_ROW.style.display = "none";
        JS_UDT_DDLVALUE_ROW.style.display = "none";
    },
    
    setTableLayout: function(){
        dojo.byId("tbQualita").colSpan = "3"
        dojo.byId("tbQualita").childNodes[0].className="lastElement"
        dojo.byId("tbMail").colSpan = "3"
        dojo.byId("tbMail").childNodes[0].className="lastElement"
        dojo.byId("tbTestoSegnalazione").colSpan = "3"
        dojo.byId("tbTestoSegnalazione").childNodes[0].className="lastElement"
        
        dojo.query("td[id*=tbTestoSollecito]").forEach(function(selectTag){
            selectTag.colSpan = "3"
            selectTag.childNodes[0].className="lastElement"
        })
        
        dojo.byId("cntlAllegati").colSpan = "3"
        dojo.byId("cntlAllegati").childNodes[0].className="lastElement"
        
        dojo.byId("tbNoteValidazione").colSpan = "3"
        dojo.byId("tbNoteValidazione").childNodes[0].className="lastElement"
        
        dojo.byId("tbTestoSoluzione").colSpan = "3"
        dojo.byId("tbTestoSoluzione").childNodes[0].className="lastElement"
        
        dojo.byId("lblSoggetto").colSpan= "4"
        dojo.byId("lblSoggetto").childNodes[0].className="lastElement lblTitolo"
        
        dojo.byId("lblLocalizzazione").colSpan= "4"
        dojo.byId("tbIndirizzoDescrizione").colSpan= "3"
        
        
        dojo.byId("lblSegnalazione").colSpan= "4"
        dojo.byId("lblSegnalazione").childNodes[0].className="lastElement lblTitolo"
        
        dojo.byId("lblSolleciti").colSpan= "4"
        dojo.byId("lblSolleciti").childNodes[0].className="lastElement lblTitolo"
        
        dojo.byId("lblAllegatii").colSpan= "4"
        dojo.byId("lblAllegatii").childNodes[0].className="lastElement lblTitolo"
        
        dojo.byId("lblValidazione").colSpan= "4"
        dojo.byId("lblValidazione").childNodes[0].className="lastElement lblTitolo"
        
        dojo.byId("lblSoluzione").colSpan= "4"
        dojo.byId("lblSoluzione").childNodes[0].className="lastElement lblTitolo"
        
        dojo.byId("lblAssegnazione").colSpan= "4"
        dojo.byId("lblAssegnazione").childNodes[0].className="lastElement lblTitolo"
        
        dojo.byId("tbIndirizzo").colSpan= "2"
        
        dojo.query("input[id$='CIVICO']").forEach(function(selectTag){
            selectTag.style.width = "50px";
        })
        
        dojo.query("input[id$='ESPONENTE']").forEach(function(selectTag){
            selectTag.style.width = "30px";
        })
        dojo.query("input[id$='LOCALIZZAZIONE']").forEach(function(selectTag){
            selectTag.style.width = "98%";
        })
        this.ShowHideSottocategoria();
        
    },
    
    ShowHideSottocategoria: function(){
      dojo.query("select[id*='SOTTOCATEGORIA']").forEach(function(selectTag){
            selectTag.style.display = "none";
        });
        
        var cat = dojo.query("select[id*='_CATEGORIA']")[0];
        
      dojo.query("select[id*='" + cat.options[cat.selectedIndex].text.toUpperCase() + "']").forEach(function(selectTag){
            selectTag.style.display = "";
        });
    },
    
    HideSection: function(){
        if (JS_URLPARAMS.PK_Report == 51 || JS_URLPARAMS.PK_Report == 50) //Sono nel primo inserimento o in validazione
        {
            dojo.byId("trIntestazioneSoluzione").style.display = "none";         
            dojo.byId("trSoluzione").style.display = "none"; 
            // dojo.byId("trSoluzioneAssegnazione").style.display = "none"; 
           
            if (JS_URLPARAMS.PK_Report == 51) //Sono nel primo inserimento
            {
                dojo.byId("trIntestazioneValidazione").style.display = "none"; 
                dojo.byId("trStatoValidazione").style.display = "none"; 
                dojo.byId("trDateLavori").style.display = "none"; 
                dojo.byId("trNoteValidazione").style.display = "none"; 
                
            }
        }
    }
    
})

