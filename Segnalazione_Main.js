/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


dojo.registerModulePath("segnalazioni", "../../../Javascript/Segnalazione");

dojo.provide("segnalazioni.Segnalazione_Main");
dojo.require("segnalazioni.TblControl");

dojo.declare("segnalazioni.Segnalazione_Main", null, {
    ddlModalitaRicezione:null,
    ddlPriorita:null,
    ddlStatoValidazione:null,
    ddlAssegnaA:null,
    ddlStatoSoluzione:null,
    divContainer:null,
    tblControl: null,
    tbStatoSoluzione: null,
    tbModalitaRicezione: null,
    tbPriorita: null,
    tbStatoValidazione: null,
    tbAssegnaA: null,
    constructor:function(){
        var localThis = this;
        this.ddlModalitaRicezione = JS_UDT_RICEZIONE[0][0]
        this.divContainer = dojo.byId("DynamicInput")
        this.tbModalitaRicezione = igedit_getById(JS_UDT_DDLVALUE[0][0].id)
        this.tbPriorita = igedit_getById(JS_UDT_DDLVALUE[0][1].id)
        this.tbStatoValidazione = igedit_getById(JS_UDT_DDLVALUE[0][2].id)
        this.tbAssegnaA = igedit_getById(JS_UDT_DDLVALUE[0][3].id)
        this.tbStatoSoluzione = igedit_getById(JS_UDT_DDLVALUE[0][4].id)
        invokeAjax("exec_ws_method", {
            "uidBasePage": uid_basepage, 
            "funcName":"spSGZ_getModalitaRicezione", 
            "funcParameters":""
        },  
        function(data, request){
            for (var i = 0; i < data.Table.length; i++)
                createDomElement("option",
                {
                    "value":data.Table[i].PK_Modalita
                },
                null,
                data.Table[i].Descrizione,
                localThis.ddlModalitaRicezione);  
			  
        } 
        , null, "IMMEDIATELY", "DynamicInput.aspx", "json", 30000, true)
        
        dojo.connect(this.ddlModalitaRicezione , "onchange", function(evt) {
            localThis.tbModalitaRicezione.setValue(localThis.ddlModalitaRicezione.selectedIndex);
       
        });
        this.ddlModalitaRicezione.selectedIndex = this.tbModalitaRicezione.getValue()
        
        this.ddlPriorita = JS_UDT_SEGNALAZIONE[0][1]
        invokeAjax("exec_ws_method", {
            "uidBasePage": uid_basepage, 
            "funcName":"spSGZ_getPrioritaSegnalazione", 
            "funcParameters":""
        },  
        function(data, request){
            for (var i = 0; i < data.Table.length; i++)
                createDomElement("option",
                {
                    "value":data.Table[i].PK_Priorita
                },
                null,
                data.Table[i].Descrizione,
                localThis.ddlPriorita);  
			  
        } 
        , null, "IMMEDIATELY", "DynamicInput.aspx", "json", 30000, true)
        
        dojo.connect(this.ddlPriorita , "onchange", function(evt) {
            localThis.tbPriorita.setValue(localThis.ddlPriorita.selectedIndex);
       
        });
        this.ddlPriorita.selectedIndex = this.tbPriorita.getValue()
        var p = [{
            "type": "System.String2",
            "value": "V"
        }];
        
        this.ddlStatoValidazione = JS_UDT_VALIDAZIONE[0][0]
        invokeAjax("exec_ws_method", {
            "uidBasePage": uid_basepage, 
            "funcName":"spSGZ_getStato", 
            "funcParameters":dojo.toJson(p)
        },  
        function(data, request){
            for (var i = 0; i < data.Table.length; i++)
                createDomElement("option",
                {
                    "value":data.Table[i].PK_Stato
                },
                null,
                data.Table[i].Descrizione,
                localThis.ddlStatoValidazione);  
			  
        } 
        , null, "IMMEDIATELY", "DynamicInput.aspx", "json", 30000, true)
        
        dojo.connect(this.ddlStatoValidazione , "onchange", function(evt) {
            localThis.tbStatoValidazione.setValue(localThis.ddlStatoValidazione.selectedIndex);
       
        });
        
        this.ddlStatoValidazione.selectedIndex = this.tbStatoValidazione.getValue();
        
        var p = [{
            "type": "System.Int32",
            "value": 6
        },
        {
            "type": "System.Int32",
            "value": 2
        }
        ];
        
        this.ddlAssegnaA= JS_UDT_SOLUZIONE[0][1]
        invokeAjax("exec_ws_method", {
            "uidBasePage": uid_basepage, 
            "funcName":"spSGZ_getUserFromRoleAndSettore", 
            "funcParameters":dojo.toJson(p)
        },  
        function(data, request){
            for (var i = 0; i < data.Table.length; i++)
                createDomElement("option",
                {
                    "value":data.Table[i].PK_Utente
                },
                null,
                data.Table[i].Descrizione,
                localThis.ddlAssegnaA);  
			  
        } 
        , null, "IMMEDIATELY", "DynamicInput.aspx", "json", 30000, true)
        
        dojo.connect(this.ddlAssegnaA , "onchange", function(evt) {
            localThis.tbAssegnaA.setValue(localThis.ddlAssegnaA.options[localThis.ddlAssegnaA.options.selectedIndex].value);
       
        });
        
        for (var i = 0; i < this.ddlAssegnaA.options.length; i++){
            if (localThis.ddlAssegnaA.options[i].value == this.tbAssegnaA.getValue())
                this.ddlAssegnaA.selectedIndex = i
        }
     //    = this.tbAssegnaA.getValue();
        
        var p = [{
            "type": "System.String2",
            "value": "S"
        }];
        
        this.ddlStatoSoluzione= JS_UDT_SOLUZIONE[0][2]
        invokeAjax("exec_ws_method", {
            "uidBasePage": uid_basepage, 
            "funcName":"spSGZ_getStato", 
            "funcParameters":dojo.toJson(p)
        },  
        function(data, request){
            for (var i = 0; i < data.Table.length; i++)
                createDomElement("option",
                {
                    "value":data.Table[i].PK_Stato
                },
                null,
                data.Table[i].Descrizione,
                localThis.ddlStatoSoluzione);  
			  
        } 
        , null, "IMMEDIATELY", "DynamicInput.aspx", "json", 30000, true)
        
        dojo.connect(this.ddlStatoSoluzione , "onchange", function(evt) {
            localThis.tbStatoSoluzione.setValue(localThis.ddlStatoSoluzione.selectedIndex);
       
        });
        
        this.ddlStatoSoluzione.selectedIndex = this.tbStatoSoluzione.getValue();
        
        this.tblControl= new segnalazioni.TblControl()  
        dojo.place(this.tblControl.table, this.divContainer, "first")
    }
    
   
    
})

dojo.addOnLoad(function(e)
{
    if (JS_URLPARAMS.PK_Report == 51)
    {
        if (JS_OBJECT_TO_COMMIT == "") // devo creare l'object to commit e reindirizzare a quel nuovo report'
        {
            var url= "LoginIntoSite.aspx?"
            for (i in JS_URLPARAMS) {
                url += i + "=" + JS_URLPARAMS[i] + "&" // shows getMethods, but not private methods
            }
            invokeAjax("exec_ws_method", {
                "uidBasePage": uid_basepage, 
                "funcName":"sp_getNextPK_ReportRicevuto", 
                "funcParameters":""
            },  
            function(data, request){
                if (data && data.Table && data.Table.length > 0)
                {
                    window.location = url + "URLACCESSPARAM1=" + data.Table[0].NextPK
                } 
            }
            , null, "IMMEDIATELY", "DynamicInput.aspx", "json", 30000, true)
        }
    }

    segnalaz = new segnalazioni.Segnalazione_Main();
    segnalaz.tblControl.HideCityFromsControl();
    segnalaz.tblControl.setTableLayout()
    segnalaz.tblControl.HideSection()
// segnalaz.showTable()
})
